import { Component, OnInit } from '@angular/core';


import { UserService } from '../../app/user.service'

@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.css']
})
export class BarchartComponent implements OnInit {

  customers: ArrayBuffer;
  data: any;
  res: any;
  labels:any;
  default:any;

  constructor(private us: UserService) { }


  ngOnInit() {


    this.reloadData();
  }
  reloadData() {
   this.us.getCustomer(this.data)
    .subscribe(res=>{
      console.log(res);
     
      this.customers = res;
      console.log(this.customers['labels']);
      console.log(this.customers['default']);
    })
    
  }



  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels:string[] = ['2019'];

  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData:number[] = [65, 59, 80, 81, 56, 55, 40

  ];


public pieChartType:string = 'pie';
public pieChartOptions:any = {'backgroundColor': [
             "#FF6384",
          "#4BC0C0",
          "#FFCE56",
          "#E7E9ED",
          "#36A2EB"
          ]}


          // events on slice click
public chartClicked(e:any):void {
  console.log(e);
}

// event on pie chart slice hover
public chartHovered(e:any):void {
  console.log(e);
}


  



}
