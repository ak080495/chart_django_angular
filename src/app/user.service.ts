import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = 'http://localhost:8000';

  constructor(private http: HttpClient) { }



  getCustomer(data) {
    return this.http.get(`${this.baseUrl}/api/chart/data/`,data);
  }
}
