
from django.urls import path,include

from rest_framework import routers
from . import views

router = routers.SimpleRouter()

router.register(r'api/users', views.UserViewSet)
# router.register(r'file', views.FileViewSet)


from .views import ChartData
urlpatterns =[
    path('',include(router.urls)),
     path('api/chart/data/', ChartData.as_view()),
]